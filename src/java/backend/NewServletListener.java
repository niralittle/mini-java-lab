package backend;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
 
/**
 * Web application life-cycle listener.
 *
 * @author nira
 */
 public class NewServletListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent sce) {
        try {
            DataAccessObject myDAO = new DataAccessObject();
            sce.getServletContext().setAttribute("myDAO", myDAO); // Application scope attribute
        } catch (Exception ex) {
            throw new RuntimeException("Can't create connection pool", ex);
        }
    }

    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("myDAO", null);
    }
}
