<%-- 
    Document   : index
    Created on : 12 бер 2014, 17:12:33
    Author     : nira
--%>

<%@page import="backend.Department"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="backend.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>La-la-la</title>
    </head>
    <body>
        <h1>Departments:</h1>
        <jsp:useBean class="backend.DataAccessObject" id="myDAO" scope="application"/>
        <table border="1">
            <%
                        for (Department e : myDAO.getAllDepts()) {
            %>
            <tr>
                <td><%=e.getDeptno()%></td>
                <td><%=e.getDname()%></td>
                <td><%=e.getLoc()%></td>
            </tr>
            <%
                        }
            %>
        </table>
    </body>
</html>